import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners(ScreenshotValidation.class)
public class ValidateLoginTest {

    private WebDriver driver;

    @BeforeClass
    public void setUp(ITestContext context) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        context.setAttribute("WebDriver", driver);
    }

    @DataProvider(name = "DataProvider")
    public Object[][] DataMethod() {
        return new Object[][]{{"kameswart@testvagrant.com", "Kameswar@12"}
        };
    }


    @Test(dataProvider="DataProvider")
    public void LoginTest(String UserName,String Passwd) {
        driver.get("http://practice.automationtesting.in/");
        driver.findElement(By.id("menu-item-50")).click();
        driver.findElement(By.id("username")).sendKeys(UserName);
        driver.findElement(By.id("password")).sendKeys(Passwd);
        driver.findElement(By.name("logi")).click();
        Assert.assertTrue(driver.findElement(By.linkText("Dashboard")).isDisplayed());
        driver.findElement(By.linkText("Automation Practice Site")).click();
    }

    @AfterClass
    public void tearDown() {
        if(driver!=null)
            driver.quit();
    }
}

